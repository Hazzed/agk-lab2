let c = document.getElementById("lsystemCanvas");
let context = c.getContext("2d");

// Stałe
const CANVAS_WIDTH = 1200;
const CANVAS_HEIGHT = 500;

const STARTING_POSITION_X = CANVAS_WIDTH / 4;
const STARTING_POSITION_Y = CANVAS_HEIGHT / 3;

// Zmienne - długość, kąt, liczba iteracji, minimalna i maksymalna losowa odległość, minimalny i maksymalny losowy kąt
let length = 20;
let deltaAngle = 120;
let iterations = 4;
let minRandomLength = 1;
let maxRandomLength = 10;
let minRandomAngle = -340;
let maxRandomAngle = 340;

// Funkcja losująca
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

// Klasa LSystemu
class LSystem {
  constructor() {
    // Obiekt definiujący symbole oraz możliwe operacje żółwia.
    this.vocabulary = [
      {
        symbol: "F",
        definition: "idź do przodu o ustaloną odległość rysując linię",
        command(turtle) {
          turtle.moveAndDraw(length);
        },
      },
      {
        symbol: "A",
        definition: "skręć o losowy kąt",
        command(turtle) {
          turtle.addAngle(getRandomInt(minRandomAngle, maxRandomAngle));
        },
      },
      {
        symbol: "G",
        definition: "idź do przodu o losową odległość rysując linię",
        command(turtle) {
          turtle.moveAndDraw(getRandomInt(minRandomLength, maxRandomLength));
        },
      },
      {
        symbol: "f",
        definition: "idź do przodu o ustaloną odległość nie rysując linii",
        command(turtle) {
          turtle.move(length);
        },
      },
      {
        symbol: "+",
        definition: "skręć w prawo o ustalony kąt",
        command(turtle) {
          turtle.addAngle(deltaAngle);
        },
      },
      {
        symbol: "-",
        definition: "skręć w lewo o ustalony kąt",
        command(turtle) {
          turtle.addAngle(-deltaAngle);
        },
      },
      {
        symbol: "[",
        definition: "odłóż bieżący stan (pozycja i rotacja) na stos",
        command(turtle) {
          turtle.pushStack();
        },
      },
      {
        symbol: "]",
        definition: "pobierz stan ze szczytu stosu",
        command(turtle) {
          turtle.popStack();
        },
      },
    ];
    // Aksjomat
    this.axiom = "F+F+F";
    // Reguły
    this.rules = {
      F: "F+F-F-F+F",
      f: "",
      A: "",
      G: "",
      ["+"]: "",
      ["-"]: "",
      ["["]: "",
      ["]"]: "",
    };
    // Ciąg znaków reprezentujący aktualną iterację L-Systemu. Początkowo jest to aksjomat
    this.lsystem = this.axiom;
    // Obiekt żółwia wykorzystywany przy rysowaniu
    this.turtle = new Turtle();
  }

  // Metoda zwracająca regułę przypisaną do symbolu, jeśli takowej nie ma zwraca sam symbol. W przypadku gdy symbol nie jest zdefiniowany zwracany jest pusty string.
  applyRule(symbol) {
    const rule = this.rules[symbol];
    if (rule !== undefined) {
      if (rule === "") return symbol;
      else return rule;
    } else if (this.findSymbolInVocabulary(symbol)) {
      return symbol;
    } else {
      console.error(`Symbol ${symbol} is not recognized by LSystem`);
      return "";
    }
  }

  // Metoda znajdująca symbol w obiekcie zawierającym definicję symboli
  findSymbolInVocabulary(symbol) {
    const searchedSymbol = this.vocabulary.find(
      (value) => value.symbol === symbol
    );
    if (searchedSymbol !== undefined) return searchedSymbol;
    else {
      console.error(`Could not find symbol ${symbol}`);
      return undefined;
    }
  }

  // Metoda robiąca następną iterację L-Systemu. Tworzy nowy string na bazie poprzedniej iteracji oraz dodaje do niego kolejne reguły.
  nextIteration() {
    let nextIteration = "";
    for (const symbol of this.lsystem) {
      nextIteration += this.applyRule(symbol);
    }
    this.lsystem = nextIteration;
  }

  // Funkcja uruchamiająca metodę nextIteration() odpowiednią liczbę razy podaną przez parametr.
  generateLSystem(iterations) {
    this.turtle = new Turtle();
    for (let i = 0; i < iterations; i++) {
      this.nextIteration();
    }
  }

  // Funkcja rysująca. Przechodzi przez string symboli i odszukuje odpowiednią dla danego symbolu operację, po czym wykonuję ją z wykorzystaniem żółwia.
  drawLSystem() {
    for (const symbol of this.lsystem) {
      this.findSymbolInVocabulary(symbol).command(this.turtle);
    }
  }
}

// Klasa żółwia, używana jest do poruszania się po płótnie
class Turtle {
  constructor() {
    // Stan zawierający pozycję oraz kąt
    this.state = {
      x: STARTING_POSITION_X,
      y: STARTING_POSITION_Y,
      angle: 0,
    };
    // Stos przechowujący stany zółwia
    this.stack = [];
  }

  // Ruch bez rysowania
  move(length) {
    const x =
      this.state.x + Math.cos((this.state.angle * Math.PI) / 180) * length;
    const y =
      this.state.y + Math.sin((this.state.angle * Math.PI) / 180) * length;
    context.moveTo(x, y);
    this.state.x = x;
    this.state.y = y;
  }

  // Ruch z rysowaniem
  moveAndDraw(length) {
    context.moveTo(this.state.x, this.state.y);
    const x =
      this.state.x + Math.cos((this.state.angle * Math.PI) / 180) * length;
    const y =
      this.state.y + Math.sin((this.state.angle * Math.PI) / 180) * length;
    context.lineTo(x, y);
    context.stroke();

    this.state.x = x;
    this.state.y = y;
  }

  // Zmiana kątu
  addAngle(angle) {
    this.state.angle += angle;
  }

  // Wrzucenie stanu na stos
  pushStack() {
    this.stack.push({
      x: this.state.x,
      y: this.state.y,
      angle: this.state.angle,
    });
  }

  // Pobranie stanu ze stosu
  popStack() {
    this.state = this.stack.pop();
    context.moveTo(this.state.x, this.state.y);
  }
}

// Funkcja zajmująca się generacją oraz rysowaniem LSystemu.
function createAndDrawLSystem() {
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
  context.beginPath();
  lsystem.lsystem = lsystem.axiom;
  lsystem.turtle = new Turtle();
  lsystem.generateLSystem(iterations);
  lsystem.drawLSystem();
}

// Funkcja wykonująca się po naciśnięciu przycisku "Akceptuj reguły". Ustawia odpowiednie reguły i wywołuje createAndDrawLSystem()
function changeRules() {
  lsystem.rules["F"] = document.getElementById("uppercaseF").value;
  lsystem.rules["f"] = document.getElementById("lowercaseF").value;
  lsystem.rules["+"] = document.getElementById("plus").value;
  lsystem.rules["-"] = document.getElementById("minus").value;
  lsystem.rules["["] = document.getElementById("sqrBracketLeft").value;
  lsystem.rules["]"] = document.getElementById("sqrBracketRight").value;
  lsystem.rules["A"] = document.getElementById("A").value;
  lsystem.rules["G"] = document.getElementById("G").value;

  createAndDrawLSystem();
}

// Funkcja wykonująca się po naciśnięciu przycisku "Akceptuj parametry". Ustawia odpowiednie parametry i wywołuje createAndDrawLSystem()
function changeParams() {
  lsystem.axiom = document.getElementById("axiom").value;
  length = document.getElementById("length").value;
  deltaAngle = parseFloat(document.getElementById("angle").value);
  iterations = document.getElementById("iterations").value;
  minRandomLength = parseFloat(
    document.getElementById("minRandomLength").value
  );
  maxRandomLength = parseFloat(
    document.getElementById("maxRandomLength").value
  );
  minRandomAngle = parseFloat(document.getElementById("minRandomAngle").value);
  maxRandomAngle = parseFloat(document.getElementById("maxRandomAngle").value);

  createAndDrawLSystem();
}

// Funkcja inicjująca, uzupełnia odpowiednie tabele w HTML i rysuje podstawowy LSystem
function init() {
  document.getElementById(
    "uppercaseFDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("F").definition;
  document.getElementById(
    "lowercaseFDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("f").definition;
  document.getElementById(
    "plusDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("+").definition;
  document.getElementById(
    "minusDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("-").definition;
  document.getElementById(
    "sqrBracketLeftDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("[").definition;
  document.getElementById(
    "sqrBracketRightDesc"
  ).innerHTML = lsystem.findSymbolInVocabulary("]").definition;
  document.getElementById("ADesc").innerHTML = lsystem.findSymbolInVocabulary(
    "A"
  ).definition;
  document.getElementById("GDesc").innerHTML = lsystem.findSymbolInVocabulary(
    "G"
  ).definition;

  document.getElementById("uppercaseF").value = lsystem.rules["F"];
  document.getElementById("lowercaseF").value = lsystem.rules["f"];
  document.getElementById("plus").value = lsystem.rules["+"];
  document.getElementById("minus").value = lsystem.rules["-"];
  document.getElementById("sqrBracketLeft").value = lsystem.rules["["];
  document.getElementById("sqrBracketRight").value = lsystem.rules["]"];
  document.getElementById("A").value = lsystem.rules["A"];
  document.getElementById("G").value = lsystem.rules["G"];

  document.getElementById("axiom").value = lsystem.axiom;
  document.getElementById("length").value = length;
  document.getElementById("angle").value = deltaAngle;
  document.getElementById("iterations").value = iterations;
  document.getElementById("minRandomLength").value = minRandomLength;
  document.getElementById("maxRandomLength").value = maxRandomLength;
  document.getElementById("minRandomAngle").value = minRandomAngle;
  document.getElementById("maxRandomAngle").value = maxRandomAngle;

  createAndDrawLSystem();
}

// Globalny obiekt LSystemu
const lsystem = new LSystem();
// Wywołanie funkcji inicjującej
init();
